import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;



/**
 *
 * @author Marc
 *
 */
public class Eurorechner {

	/**
	 * 
	 */
    private static final double KURS = 1.29535;

    /**
     * 
     */
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    /**
     * 
     */
    private DecimalFormat decimalFormat = new DecimalFormat("#0.00");


    /**
     * The method is asking for the Letters E(e) or U(u) and returns the 
     * requested currency.
     * 
     * @return The Letter which represents the currency. Represented by a String
     * 
     * @throws IOException
     */
    private String readCurrencyFromConsole() throws IOException {
    	String currency = null;
    	boolean isValidCurrency = false;
    	int numberOfAttempts = 0;

    	// Is looping as long as user has not entered E(e) or U(u)
    	while (!isValidCurrency) {
    		if (numberOfAttempts == 0) {
    			System.out.println("Eingabe der Ausgangswaehrung (E)uro oder (U)S Dollar: ");
    		} else {
    			System.out.println("Bitte nur das Geforderte eingeben: ");
    		}

    		currency = br.readLine();
    		numberOfAttempts++;

    		if (currency.equalsIgnoreCase("u") || currency.equalsIgnoreCase("e")) {
    			isValidCurrency = true;
    		}
    	}
    	return currency;
    }


    /**
     * The method is asking if the user want to continue the program. By entering j or n the decision can be made.
     * 
     * @return The Letter j for yes or n for no. 
     * 
     * @throws IOException if somethings going wrong while input/output
     */
    private boolean continueExchangeCurrency() throws IOException {
        boolean wantToContinue = false;
        boolean isValidInput = false;
        String input = "";
        int numberOfAttempts = 0;

        while (!isValidInput) {

        	if (numberOfAttempts == 0) {
        		System.out.println("Wollen Sie noch einmal j/n? :");
        	} else {
        		System.out.println("Bitte nur j oder n eingeben: ");
        	}

        	input = br.readLine();
        	numberOfAttempts++;

        	if (input.equalsIgnoreCase("j")) {
        		isValidInput = true;
        		wantToContinue = true;
        	} else {
        		isValidInput = true;
        	}
        }
        return wantToContinue;
    }


    /**
     * The method is asking the user to enter a valid value for the currency.
     * 
     * @return returns the currency with a double
     * 
     * @throws IOException
     */
    private double readCurrencyValueFromConsole() throws IOException {
        double currencyValue = 0;
        boolean isValidCurrencyValue = false;
        int numberOfAttempts = 0;

        while (!isValidCurrencyValue) {
            if (numberOfAttempts == 0) {
                System.out.println("Bitte geben Sie einen Währungsbetrag ein: ");
            } else {
                System.out.println("Bitte nur Zahlen eingeben, ohne Buchstaben oder Sonderzeichen: ");
            }

            try {
                currencyValue = Double.parseDouble(br.readLine().strip());
                isValidCurrencyValue = true;
            } catch (NumberFormatException nfe) {
                numberOfAttempts++;
            }
        }
        return currencyValue;
    }
    
    
    /**
     * The method makes the exchange from Euro to US Dollar.
     * 
     * @param euroAmount the value which will be changed to Dollar.
     * 
     * @return returns the Dollar value as a Double.
     */
    private double exchangeEuroToUSD(final double euroAmount) {
        return euroAmount * KURS;
    }

    
    /**
     * The method makes the exchange from Dollar to Euro.
     * 
     * @param usdAmount the value which will be changed to Euro
     * 
     * @return returns the Euro value as a Double
     */
    private double exchangeUSDToEuro(final double usdAmount) {
        return usdAmount / KURS;
    }
    

    /**
     * Main Method. The program starts at this point.
     * 
     * @param args
     * 
     * @throws IOException
     */
	public static void main(final String[] args) throws IOException {
		Eurorechner er = new Eurorechner();
		er.run();
	}
	

	/**
	 * 
	 * @throws IOException
	 */
	private void run() throws IOException {
		do {		
			String currency = readCurrencyFromConsole();
			double currencyValue = readCurrencyValueFromConsole();
			double changedCurrency = 0;
			
			if (currency.contentEquals("e")) {
				changedCurrency = exchangeEuroToUSD(currencyValue);
				System.out.println("Der Umgerechnete Betrag: " + decimalFormat.format(changedCurrency) + " USD");
			} else {
				changedCurrency = exchangeUSDToEuro(currencyValue);
				System.out.println("Der Umgerechnete Betrag: " + decimalFormat.format(changedCurrency) + " Euro");
			}
			
			System.out.println("-----------------------------------");
			
		} while (continueExchangeCurrency());
		
		System.out.println("<PROGRAMM> Ende");
	}

}
